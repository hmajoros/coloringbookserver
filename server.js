// server.js

// BASE SETUP
// =============================================================================
var express    = require('express');
var app        = express();                
var bodyParser = require('body-parser');
var mysql      = require('mysql');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var conn = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: 'triotriotrio',
	database: 'trio'
});

conn.connect(function(err) {
	if (err) {
		console.log('failed to connect!');
	} else {
		console.log('connected!');
	}
});
var port = process.env.PORT || 80;

// SETTING UP PIN
// =============================================================================
var PIN;
var CONNECTED = false;
var VERSION = 0;

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();         
router.use(function(req, res, next) 
{
    console.log('---------------------');
    console.log(req.url);
    next();
});

router.get('/', function(req, res) 
{
	res.json({ message: 'hooray! welcome to our api!' });   
});

router.route('/connect').post(function(req, res)
{
  console.log('POST: /connect');
	var pin = req.body.pin;
  var query = 'SELECT COUNT(*) AS count FROM environments WHERE pin = ?';
  conn.query(query, [pin], function(error, result, fields) {
    if (result[0]['count']) {
      res.json({ message: 'Error: PIN already in use' });
    } else {
      addPinQuery = 'INSERT INTO environments (pin, data, versionnum) VALUES (?, ?, ?);';
      conn.query(addPinQuery, [pin, null, 0], function(error, result, fields) {
        if (error) {
          // TODO: handle error
        } else {
	VERSION = 0;
          res.status(200).json({ message : 'PIN recieved' });
        }
      });
    }
  });

}).get(function(req, res) {
  console.log('GET: /connect');
  res.status(200).json({ connected: CONNECTED }); 
});

router.route('/environment').post(function(req, res) 
{
  // 1. get PIN from post body
  var pin = req.body.pin,
      data = JSON.stringify(req.body.data);

  console.log(data);

  // 2. check if PIN exists in database
  var query = 'SELECT versionnum FROM environments WHERE pin = ?';
  var test = conn.query(query, [pin], function(error, result, fields) {
    var count = result.length;
    if (count) {
      console.log('PIN exists');
      var versionnum = result[0]['versionnum'] + 1;
      var updateQuery = 'UPDATE environments SET versionnum = ?, data = ? WHERE pin = ?;';
      conn.query(updateQuery, [versionnum, data, pin], function(error, result, fields) {
        if (error) {
          res.json({ code: error.code, message: 'Database query failed' });
        } else {
          var msg = 'Updated environment with pin ' + pin + ' to version ' + versionnum;
          res.status(200).json({  message: msg });
        }
      });
    } else {
      console.log('PIN does not exist');
      var insertQuery = 'INSERT INTO environments (pin, data, versionnum) VALUES (?, ?, ?);' 
      conn.query(insertQuery, [pin, data, 0], function(error, result, fields) {
        var msg = 'Inserted new Environment with PIN: ' + pin;
        res.status(200).json({ message: msg }); 
      });
    }
  });
}).get(function(req, res) 
{
  // 1. get PIN + versionnum from post body
  var pin = req.query.pin;

  var query = 'SELECT versionnum, data FROM environments WHERE pin = ?';
  conn.query(query, [pin], function(error, result, fields) {
    var count = result.length;
    if (count) {
      if (VERSION < result[0]['versionnum']) {
	console.log("Sending New Version: " + result[0]['versionnum']);

	var data = JSON.parse(result[0]['data']);
      res.send(JSON.stringify({ message: "New Version", data}));
      VERSION = result[0]['versionnum'];
    } else {
	console.log("No Update");
      res.status(200).json({ message: 'No Update' });
    }
  } else {
	console.log("PIN not found");
    res.json({ message: 'PIN not found' });
  }
});


// 2. check versionnum against db version
// 3. if equal, return 304
// 4. else, return 200 with search_xml data and new versionnum
});

app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);

